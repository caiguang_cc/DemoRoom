package com.sheuni.demoroom.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.sheuni.demoroom.util.AppUtil;

@Database(entities = {User.class}, version = 1)
public abstract class AppDatabase extends RoomDatabase {

    private static AppDatabase appDatabase;

    static {
        appDatabase = Room.databaseBuilder(AppUtil.getApplication(), AppDatabase.class, "database-name")
                .allowMainThreadQueries()
                .build();
    }

    public static AppDatabase getInstance() {
        return appDatabase;
    }

    public abstract UserDao userDao();
}
