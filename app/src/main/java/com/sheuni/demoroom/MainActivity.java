package com.sheuni.demoroom;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.sheuni.demoroom.database.AppDatabase;
import com.sheuni.demoroom.database.User;

import java.util.List;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{

    private EditText firstName;
    private EditText lastName;
    private EditText age;
    private Button commit;
    private TextView textView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViewComponent();
        commit.setOnClickListener(this);
        updateTextView();
    }

    @Override
    public void onClick(View v) {
        User user = new User(firstName.getText().toString(),
                lastName.getText().toString(),
                Integer.parseInt(age.getText().toString()));

        AppDatabase.getInstance().userDao().insertAll(user);

        updateTextView();
    }

    private void initViewComponent() {
        firstName = findViewById(R.id.first_name);
        lastName = findViewById(R.id.last_name);
        age = findViewById(R.id.age);
        commit = findViewById(R.id.commit);
        textView = findViewById(R.id.textView);
    }

    private void updateTextView() {
        List<User> users = AppDatabase.getInstance().userDao().getAll();
        textView.setText(users.toString());
    }
}
