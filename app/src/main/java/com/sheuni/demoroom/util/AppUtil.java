package com.sheuni.demoroom.util;

import android.app.Application;

import java.lang.reflect.InvocationTargetException;

public class AppUtil {

    private static Application application;

    public static Application getApplication() {
        if(application == null) {
            try {
                application = (Application)Class.forName("android.app.ActivityThread")
                        .getMethod("currentApplication")
                        .invoke(null, (Object[]) null);
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            } catch (InvocationTargetException e) {
                e.printStackTrace();
            } catch (NoSuchMethodException e) {
                e.printStackTrace();
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
            }
        }
        return application;
    }
}
